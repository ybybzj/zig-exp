const std = @import("std");
const builtin = @import("builtin");
const Step = std.build.Step;
const Build = std.Build;
const Child = std.process.Child;
const join = std.fs.path.join;
const clsclr = @import("./src/clsclr.zig");
const c_style = clsclr.cls_style;
const c = clsclr.clsclr;
const ClsClr = clsclr.ClsCLr;

fn print(comptime fmt: []const u8, args: anytype) void {
    return std.debug.print(fmt ++ "\n", args);
}

pub fn build(b: *Build) !void {
    // print("Entering building...", .{});

    use_color_escapes = clsclr.is_clsclr_support();
    const header_step = PrintStep.create(b, logo);

    // const target = b.standardTargetOptions(.{});
    //
    //
    // const optimize = b.standardOptimizeOption(.{});

    const exps_step = b.step("exp", "Check all ziglings");
    b.default_step = exps_step;

    var prev_step = &header_step.step;
    const exp_name: ?[]const u8 = b.option([]const u8, "name", "Select experiment");

    const work_path = "exp";
    if (exp_name) |e_name| {
        const name = if (!std.mem.eql(u8, std.fs.path.extension(e_name), ".zig")) try concat(b.allocator, e_name, ".zig") else try b.allocator.dupe(u8, e_name);
        const verify_stepn = ExpStep.create(b, work_path, name);

        verify_stepn.step.dependOn(prev_step);

        prev_step = &verify_stepn.step;

        exps_step.dependOn(prev_step);
        return;
    }

    var iter = try cwdIter(work_path);
    while (try iter.next()) |path| {
        // print("found exp : {s}", .{path.name});
        const verify_stepn = ExpStep.create(b, work_path, try b.allocator.dupe(u8, path.name));

        verify_stepn.step.dependOn(prev_step);

        prev_step = &verify_stepn.step;
    }
    exps_step.dependOn(prev_step);
}

fn cwdIter(base: []const u8) !std.fs.IterableDir.Iterator {
    const dir = try std.fs.cwd().openIterableDir(base, .{});
    return dir.iterate();
}

pub const logo =
    \\   
    \\    _____   _          ______         
    \\   /__  /  (_)___ _   / ____/  ______ 
    \\     / /  / / __ `/  / __/ | |/_/ __ \
    \\    / /__/ / /_/ /  / /____>  </ /_/ /
    \\   /____/_/\__, /  /_____/_/|_/ .___/ 
    \\          /____/             /_/      
    \\
    \\    "Experiments in Zig!!!"
    \\
    \\
;

/// Prints a message to stderr.
const PrintStep = struct {
    step: Step,
    message: []const u8,

    pub fn create(owner: *Build, message: []const u8) *PrintStep {
        const self = owner.allocator.create(PrintStep) catch @panic("OOM");
        self.* = .{
            .step = Step.init(.{
                .id = .custom,
                .name = "print",
                .owner = owner,
                .makeFn = make,
            }),
            .message = message,
        };

        return self;
    }

    fn make(step: *Step, _: *std.Progress.Node) !void {
        const self = @fieldParentPtr(PrintStep, "step", step);

        print("{s}", .{self.message});
    }
};

const ExpStep = struct {
    step: Step,
    work_path: []const u8,
    name: []const u8,

    pub fn create(b: *Build, work_path: []const u8, name: []const u8) *ExpStep {
        const self = b.allocator.create(ExpStep) catch @panic("OOM");
        self.* = .{ .step = Step.init(.{
            .id = .custom,
            .name = name,
            .owner = b,
            .makeFn = make,
        }), .work_path = work_path, .name = name };
        return self;
    }

    fn make(step: *Step, prog_node: *std.Progress.Node) !void {
        // NOTE: Using exit code 2 will prevent the Zig compiler to print the message:
        // "error: the following build command failed with exit code 1:..."
        const self = @fieldParentPtr(ExpStep, "step", step);

        print(c(&.{ "\n", c_style("{s}", &.{ClsClr.cls_bhBlack}), "\n----------------------" }), .{self.name});
        const exe_path = self.compile(prog_node) catch {
            print(c(&.{ "\n", c_style("{s} compile failed ...", &.{ClsClr.cls_Bold}) }), .{self.name});

            self.printErrors();

            std.os.exit(2);
        };
        if (exe_path == null) {
            print(c(&.{ "\n", c_style("{s} compile failed ...", &.{ClsClr.cls_Bold}) }), .{self.name});

            self.printErrors();

            std.os.exit(2);
        }

        self.run(exe_path.?, prog_node) catch {
            print(c(&.{ "\n", c_style("{s} execution failed ...", &.{ClsClr.cls_Bold}) }), .{self.name});
            self.printErrors();

            std.os.exit(2);
        };

        // Print possible warning/debug messages.
        self.printErrors();
    }

    fn run(self: *ExpStep, exe_path: []const u8, _: *std.Progress.Node) !void {
        print("Running ...", .{});
        resetLine();

        const b = self.step.owner;

        // Allow up to 1 MB of stdout capture.
        const max_output_bytes = 1 * 1024 * 1024;

        var result = Child.exec(.{
            .allocator = b.allocator,
            .argv = &.{exe_path},
            .cwd = b.build_root.path.?,
            .cwd_dir = b.build_root.handle,
            .max_output_bytes = max_output_bytes,
        }) catch |err| {
            return self.step.fail("unable to spawn {s}: {s}", .{
                exe_path, @errorName(err),
            });
        };

        printResult(&result);
    }

    fn compile(self: *ExpStep, prog_node: *std.Progress.Node) !?[]const u8 {
        print("Compiling ...", .{});

        const b = self.step.owner;
        const exp_path = self.name;
        const path = join(b.allocator, &.{ self.work_path, exp_path }) catch
            @panic("OOM");

        var zig_args = std.ArrayList([]const u8).init(b.allocator);
        defer zig_args.deinit();

        zig_args.append(b.zig_exe) catch @panic("OOM");

        zig_args.append("build-exe") catch @panic("OOM");

        zig_args.append(b.pathFromRoot(path)) catch @panic("OOM");

        zig_args.append("--cache-dir") catch @panic("OOM");
        zig_args.append(b.pathFromRoot(b.cache_root.path.?)) catch @panic("OOM");

        zig_args.append("--listen=-") catch @panic("OOM");

        return try self.step.evalZigProcess(zig_args.items, prog_node);
    }

    fn printResult(result: *const std.ChildProcess.ExecResult) void {
        print(c(&.{ c_style("Term info", &.{ .cls_Underline, .cls_Bold }), ": {any}\n" }), .{result.term});
        print(c(&.{ c_style("Stdout", &.{.cls_Green}), " =>" }), .{});
        print("{s}", .{result.stdout});

        print(c(&.{ c_style("Stderr", &.{.cls_Cyan}), " =>" }), .{});
        print("{s}", .{result.stderr});
    }

    fn printErrors(self: *ExpStep) void {
        resetLine();
        // Display error/warning messages.
        if (self.step.result_error_msgs.items.len > 0) {
            for (self.step.result_error_msgs.items) |msg| {
                print(c(&.{ c_style("error: ", &.{ .cls_Bold, .cls_Red }), c_style("{s}", &.{.cls_hRed}) }), .{msg});
            }
        }

        // Render compile errors at the bottom of the terminal.
        // TODO: use the same ttyconf from the builder.
        const ttyconf: std.io.tty.Config = if (use_color_escapes)
            .escape_codes
        else
            .no_color;
        if (self.step.result_error_bundle.errorMessageCount() > 0) {
            self.step.result_error_bundle.renderToStdErr(.{ .ttyconf = ttyconf });
        }
    }
};

/// Clears the entire line and move the cursor to column zero.
/// Used for clearing the compiler and build_runner progress messages.
fn resetLine() void {
    if (use_color_escapes) print("{s}", .{"\x1b[2K\r"});
}
var use_color_escapes: bool = false;

fn concat(allocator: std.mem.Allocator, s1: []const u8, s2: []const u8) std.mem.Allocator.Error![]const u8 {
    const total_len = s1.len + s2.len;
    if (total_len <= 0) return &[0]u8{};

    if (s1.len <= 0) {
        return try allocator.dupe(u8, s2);
    } else if (s2.len <= 0) {
        return try allocator.dupe(u8, s1);
    } else {
        const buf = try allocator.alloc(u8, total_len);
        errdefer allocator.free(buf);
        @memcpy(buf[0..s1.len], s1);
        @memcpy(buf[s1.len..][0..s2.len], s2);
        return buf;
    }
}
