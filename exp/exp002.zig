const std = @import("std");

pub fn main() void {
    std.debug.print("executing {s}...", .{@src().fn_name});
}
